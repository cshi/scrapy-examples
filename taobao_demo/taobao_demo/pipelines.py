# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import sys
import pymysql

class TaobaoDemoPipeline(object):
    def __init__(self):
        self.conn = pymysql.connect(host='localhost',user='root',passwd='123456',
                db='scrapy',charset='utf8')

    def process_item(self, item, spider):
        try:
            title = item['title'][0].strip()
            link = item['link']
            price = item['price'][0]
            comment = item['comment'][0].strip()
            sql = "insert into tb_violin(title,link,price,comment) values( \
                    '%s','%s','%s','%s')" %(title,link,price,comment) # must use '%s' not %s to avoid space issue
            self.conn.query(sql)
            self.conn.commit()
            return item
        except Exception as err:
            print err,str(item)

    def __del__(self):
        self.close_conn()

    def close_conn(self):
        self.conn.close()
            
