# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from cassandra.cluster import Cluster

class AllitbooksPipeline(object):

    def __init__(self):
        # https://datastax.github.io/python-driver/api/cassandra/cluster.html
        self.host = '10.77.44.158'
        self.keyspace = 'webcrawl'
        self.cluster = Cluster([self.host])
        self.session = self.cluster.connect(self.keyspace)

    def process_item(self, item, spider):
        #print('item:', item)
        try:
            self.session.execute(
                """
                insert into book (name, year, page, filesize, category, author, download_url, url)
                values (%s, %s, %s, %s, %s, %s, %s, %s)
                """,
                (item['name'], item['year'], item['page'], item['filesize'], 
                    item['category'], item['author'], item['download_url'], item['url']))
        except Exception as err:
            print('error:', err, ',item: ', item)
    
    def __del__(self):
        self.cluster.close_conn()

    def close_conn():
        self.cluster.shutdown()

