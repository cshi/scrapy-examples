# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class DbbooksItem(scrapy.Item):
    # define the fields for your item here like:
    title = scrapy.Field()
    link = scrapy.Field()
    author = scrapy.Field()
    press = scrapy.Field()
    publish_date = scrapy.Field()
    price = scrapy.Field()
    star = scrapy.Field()
    comment_num = scrapy.Field()

