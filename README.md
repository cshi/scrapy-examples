## My scrapy examples

Sometimes I find it very useful to crawl some infos from interested sites, so I use scrapy to to the job. Before you can create your spider, I suggest getting to know some basic knowledge of xpath, css, very useful to capture what you need.

## Snapshot of my crawl results

![allitbooks stored in Cassandra](./images/crawl-allitbooks.png)
